[Setup]
AppName=ScreenUp
AppVerName=ScreenUp 2.2.1
AppVersion=2.2.1
AppPublisher=Karolina Pałka
AppCopyright=Copyright (C) 2022 Karolina Pałka
DefaultDirName={pf}\ScreenUp
DefaultGroupName=ScreenUp
AllowNoIcons=yes
OutputBaseFilename=screenup_2.2.1
SetupIconFile=screenup\screenupbig.ico
Compression=lzma
SolidCompression=yes
VersionInfoVersion=2.2.1
UninstallDisplayIcon={app}\screenup.exe

[Messages]
SetupWindowTitle=Setup ScreenUp 2.2.1

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "screenup\bin\Release\screenup.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "screenup\bin\Release\shot.wav"; DestDir: "{app}"; Flags: ignoreversion
Source: "screenup\bin\Release\bleep.wav"; DestDir: "{app}"; Flags: ignoreversion


; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\ScreenUp"; Filename: "{app}\screenup.exe"
Name: "{group}\{cm:UninstallProgram,ScreenUp}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\ScreenUp"; Filename: "{app}\screenup.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\screenup.exe"; Description: "{cm:LaunchProgram,ScreenUp}"; Flags: postinstall nowait skipifsilent
