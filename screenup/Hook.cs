using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace screenup
{
    class Hook
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYUP = 0x0101;
        private const int WM_SYSKEYUP = 0x0105;
        private const int VK_SNAPSHOT = 0x2C;
        private static IntPtr HookID = IntPtr.Zero;
        private static Hook.LowLevelKeyboardProc Proc = HookCallback;

        private static IntPtr SetHook(LowLevelKeyboardProc proc) {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule) {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam) {
            if (nCode >= 0) {
                if (wParam == (IntPtr)WM_SYSKEYUP) { // ALT+PRINTSCREEN
                    int vkCode = Marshal.ReadInt32(lParam);
                    if (vkCode == VK_SNAPSHOT) Stuff.KeyToScreen(Stuff.ScreenType.ActiveWindow);
                }
                else if (wParam == (IntPtr)WM_KEYUP) { // PRINTSCREEN
                    int vkCode = Marshal.ReadInt32(lParam);
                    if (vkCode == VK_SNAPSHOT) Stuff.KeyToScreen(Stuff.ScreenType.Desktop);
                }
            }
            return CallNextHookEx(HookID, nCode, wParam, lParam);
        }

        public static void HookWindows() {
            HookID = SetHook(Proc);
        }

        public static void UnhookWindows() {
            UnhookWindowsHookEx(HookID);
        }
    }
}
