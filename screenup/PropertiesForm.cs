using System;
using System.Windows.Forms;

namespace screenup
{
    public partial class PropertiesForm : Form
    {
        public PropertiesForm()
        {
            InitializeComponent();
            Icon = Properties.Resources.screenupbig;
        }

        private void UpdateDialog()
        {
            checkBox1.Checked = Stuff.ProgramConfig.AutoSave;
            textBox2.Text = Stuff.ProgramConfig.AutoSavePath;

            checkBox2.Checked = Stuff.ProgramConfig.ChangeFormat;

            checkBox5.Checked = Stuff.ProgramConfig.Sounds;
            checkBox3.Checked = Stuff.ProgramConfig.AutoOpen;

            button3.Enabled = checkBox1.Checked;
            textBox2.Enabled = checkBox1.Checked;
        }

        private void UpdateConfig()
        {
            Stuff.ProgramConfig.AutoSave = checkBox1.Checked;
            Stuff.ProgramConfig.AutoSavePath = textBox2.Text;
            Stuff.ProgramConfig.ChangeFormat = checkBox2.Checked;
            Stuff.ProgramConfig.Sounds = checkBox5.Checked;
            Stuff.ProgramConfig.AutoOpen = checkBox3.Checked;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Stuff.IsDialog = true;
            UpdateDialog();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Stuff.IsDialog = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateConfig();
            Stuff.SaveConfig();
            Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            button3.Enabled = checkBox1.Checked;
            textBox2.Enabled = checkBox1.Checked;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderBrowserDialog1.ShowNewFolderButton = true;
                textBox2.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stuff.IsDialog = false;
            Close();
        }

    }
}