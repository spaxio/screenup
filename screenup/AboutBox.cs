using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;

namespace screenup
{
    partial class AboutBox : Form
    {
        public AboutBox()
        {
            InitializeComponent();

            Icon = Properties.Resources.screenupbig;
            this.label2.Text = "Version " + AssemblyVersion;
            this.label3.Text = AssemblyCopyright;
        }

        #region Assembly Attribute Accessors

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                // Get all Copyright attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                // If there aren't any Copyright attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Copyright attribute, return its value
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }
        #endregion

        private void AboutBox_Load(object sender, EventArgs e)
        {
            Stuff.IsDialog = true;
        }

        private void AboutBox_FormClosed(object sender, FormClosedEventArgs e)
        {
            Stuff.IsDialog = false;
        }

        private void AboutBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            e.Graphics.DrawString(label1.Text, label1.Font, new SolidBrush(label1.ForeColor), new PointF(label1.Location.X, label1.Location.Y));
        }
    }
}
