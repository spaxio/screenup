using System;
using System.Drawing;
using System.Drawing.Imaging;

using System.Runtime.InteropServices;
using System.Xml.Serialization;

using System.Threading;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Net;

using System.Web.Script.Serialization;

namespace screenup
{
    public partial class Stuff
    {

        #region Static strings
        public static string[] TabString = {
                                                "The program was unable to make a screenshot.", // 1 0
                                                "Can't send image. Imgur is unavailable.", // 3 1
                                                "ScreenUp - Error!", // 9 2
                                                "yyy-MM-dd", // 14 3
                                           };
        #endregion

        #region Global Enums
        public enum ScreenType
        {
            ActiveWindow,
            Desktop
        }

        public enum ScreenFormatEnum
        {
            PNG,
            JPG
        }
        #endregion

        public static string ExeDirectory = Application.StartupPath;
        public static string AppDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\ScreenUp";
        public static bool IsDialog = false;

        public static string ProgramVersion = null;

        public static float Scale = 2;

        public static Size DisplaySize;
        public static Size ScreenSize;
        public static Size WindowSize;

        public static Config ProgramConfig = new Config();
        public static MainForm MainForm = null;

        #region Private functions
        private static string GetRandomString()
        {
            string result = "";
            Random rand = new Random((int)DateTime.Now.Ticks);

            for (int x = 0; x < 9; x++)
            {
                result += (char)rand.Next('a', 'z');
            }

            return result;
        }

        private static ImageCodecInfo GetEncoderInfo(string mimetype)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimetype)
                    return encoders[j];
            }
            return null;
        }
        #endregion

        #region Win32 Clip By KrystianD
        [DllImport("User32.dll")]
        public static extern Boolean OpenClipboard(IntPtr hWndNewOwner);
        [DllImport("User32.dll")]
        public static extern Boolean EmptyClipboard();
        [DllImport("User32.dll")]
        public static extern Boolean CloseClipboard();
        [DllImport("User32.dll")]
        public static extern IntPtr SetClipboardData(int uFormat, IntPtr hMem);

        public static void ClipboardSetText(String text)
        {
            if (OpenClipboard(IntPtr.Zero))
            {
                EmptyClipboard();
                SetClipboardData(13, Marshal.StringToHGlobalUni(text));
                CloseClipboard();
            }
        }
        #endregion

        #region Public functions
        public static int ClipValue(int value, int min, int max)
        {
            if (value > max) return max;
            else if (value < min) return min;
            else return value;
        }

        public static string FormatToString(ScreenFormatEnum sf)
        {
            return ((sf == ScreenFormatEnum.PNG) ? "png" : "jpg");
        }

        public static string CreateFileFolderError(string path)
        {
            return "Can't create file " + Path.GetFileName(path) + " in " + Path.GetDirectoryName(path);
        }

        public static Size CalcAspectRatio(Size originalsize, Size field)
        {
            float width = (float)field.Width;
            float height = (float)field.Height;
            float aspectratio = width / height;

            float picwidth = (float)originalsize.Width;
            float picheight = (float)originalsize.Height;
            float picaspectratio = picwidth/picheight;

            if (picaspectratio > aspectratio)
            {
                int newheight = (int)(width/picwidth*picheight);
                return new Size(field.Width, newheight);
            }
            else if (picaspectratio < aspectratio)
            {
                int newwidth =  (int)(height/picheight*picwidth);
                return new Size(newwidth, field.Height);
            }

            return field;
        }

        public static void EmptyCache()
        {
            string cachedirectory = Stuff.AppDirectory + "\\cache";
            if (Directory.Exists(cachedirectory))
            {
                Directory.Delete(cachedirectory, true);
                Directory.CreateDirectory(cachedirectory);
            }
            else
            {
                Directory.CreateDirectory(cachedirectory);
            }
        }

        public static bool LoadConfig()
        {
            try
            {
                if (File.Exists(AppDirectory + "\\config2.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Config));
                    Stream reader = File.Open(AppDirectory + "\\config2.xml", FileMode.Open);
                    Stuff.ProgramConfig = (Stuff.Config)serializer.Deserialize(reader);
                    reader.Close();
                }
                else throw new Exception();
            }
            catch { Stuff.ProgramConfig = new Config(); return false; }

            return true;
        }

        public static void SaveConfig()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));
                Stream writer = File.Open(AppDirectory + "\\config2.xml", FileMode.Create);
                serializer.Serialize(writer, Stuff.ProgramConfig);
                writer.Close();
            }
            catch { }
        }

        public static string GetCacheRandomName(string ext)
        {
            string local = AppDirectory + "\\cache\\" + GetRandomString();
            local += ext;
            while (File.Exists(local))
            {
                local = AppDirectory + "\\cache\\" + GetRandomString();
                local += ext;
            }

            return local;
        }
        #endregion

        #region Classes
        [Serializable]
        public class Config
        {
            #region Fields
            private int pThumbnailCount = 7;
            private bool pAutoSave = true;
            private string pAutoSavePath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "\\screenup";
            private string pNameFormat = TabString[3];
            private bool pChangeFormat = false;
            private ScreenFormatEnum pScreenFormat = ScreenFormatEnum.JPG;
            private bool pSounds = true;
            private bool pAutoOpen = true;
            #endregion

            #region Properties
            public int ThumbnailCount { get { return pThumbnailCount; } set { pThumbnailCount = ClipValue(value, 1, 16); } }
            public bool ChangeFormat { get { return pChangeFormat; } set { pChangeFormat = value; } }
            public bool AutoSave { get { return pAutoSave; } set { pAutoSave = value; } }
            public string AutoSavePath { get { return pAutoSavePath; } set { pAutoSavePath = value; } }
            public string NameFormat
            {
                get { return pNameFormat; }
                set
                {
                    try
                    {
                        pNameFormat = DateTime.Now.ToString(value);
                        if (pNameFormat.Length == 0) throw new Exception();
                        if (pNameFormat.IndexOfAny(Path.GetInvalidFileNameChars()) != -1) throw new Exception();
                    }
                    catch { pNameFormat = TabString[3]; return; }
                    pNameFormat = value;
                }
            }
            public ScreenFormatEnum ScreenFormat { get { return pScreenFormat; } set { pScreenFormat = value; } }
            public bool Sounds { get { return pSounds; } set { pSounds = value; } }
            public bool AutoOpen { get { return pAutoOpen; } set { pAutoOpen = value; } }
            #endregion
        }
        #endregion

        #region WebRequest
        public static void PostFile(string url, string filename, out string outlink, BackgroundWorker worker)
        {
            byte[] image;
            using (FileStream file = new FileStream(filename, FileMode.Open))
            {
                image = new byte[file.Length];
                file.Read(image, 0, image.Length);
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;
            request.Method = "POST";
            request.ContentType = "image/*";
            request.ContentLength = image.Length;
            request.UserAgent = "ScreenUp/" + ProgramVersion;
            request.Headers.Add("Authorization", "Client-ID d370af35ee568c3");

            using (MemoryStream formStream = new MemoryStream(image))
            {
                using (Stream datastream = request.GetRequestStream())
                {
                    int lastRead = 0;
                    byte[] buffer = new byte[32768];

                    while ((lastRead = formStream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        datastream.Write(buffer, 0, lastRead);
                        worker.ReportProgress((int)(formStream.Position * 100 / formStream.Length));
                    }
                }
            }

            WebResponse response = request.GetResponse();
            StreamReader responetext = new StreamReader(response.GetResponseStream());
            string json = responetext.ReadToEnd();

            var imgur = new JavaScriptSerializer().Deserialize<ImgurResponse>(json);

            if (imgur.success && imgur.status == 200)
            {
                outlink = imgur.data.link;
            }
            else
            {
                throw new Exception("Upload error: " + imgur.status.ToString());
            }
        }

        public class Data_
        {
            public string id { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string datetime { get; set; }
            public string type { get; set; }
            public bool animated { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public int size { get; set; }
            public int views { get; set; }
            public int bandwidth { get; set; }
            public string nsfw { get; set; }
            public string section { get; set; }
            public string deletehash { get; set; }
            public string link { get; set; }
        }

        public class ImgurResponse
        {
            public Data_ data { get; set; }
            public bool success { get; set; }
            public int status { get; set; }
        }

        #endregion

        #region ScreenMaker
        public struct WINDOWINFO
        {
            public UInt32 cbSize;
            public Rectangle rcWindow;
            public Rectangle rcClient;
            public UInt32 dwStyle;
            public UInt32 dwExStyle;
            public UInt32 dwWindowStatus;
            public uint cxWindowBorders;
            public uint cyWindowBorders;
            public Int16 atomWindowType;
            public Int16 wCreatorVersion;
        }

        [Flags]
        public enum SoundFlags : int
        {
            SND_ASYNC = 0x0001,
            SND_FILENAME = 0x00020000
        }

        public enum SWC
        {
            SW_HIDE = 0, SW_SHOWNORMAL = 1, SW_NORMAL = 1, SW_SHOWMINIMIZED = 2, SW_SHOWMAXIMIZED = 3, SW_MAXIMIZE = 3,
            SW_SHOWNOACTIVATE = 4, SW_SHOW = 5, SW_MINIMIZE = 6, SW_SHOWMINNOACTIVE = 7, SW_SHOWNA = 8, SW_RESTORE = 9,
            SW_SHOWDEFAULT = 10, SW_FORCEMINIMIZE = 11, SW_MAX = 11
        };

        [DllImport("winmm.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        public static extern bool PlaySound(string pszSound, IntPtr hMod, SoundFlags sf);

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("User32.dll")]
        public static extern int ShowWindowAsync(IntPtr hWnd, int swCommand);

        [DllImport("user32.dll")]
        public static extern bool GetWindowInfo(IntPtr hwnd, out WINDOWINFO lpwindowinfo);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll")]
        private static extern bool GetWindowRect(IntPtr hwnd, out Rectangle lprect);

        public static Image MakeScreen(ScreenType st)
        {
            Image image = null;
            Graphics graph = null;
            Rectangle nrect = new Rectangle();

            if (st == ScreenType.ActiveWindow)
            {
                // Pobieramy aktywne okno
                IntPtr hwnd = GetForegroundWindow();

                // Pobierwamy wskaznik na prostokat aktywnego okna
                Rectangle rect = new Rectangle();
                GetWindowRect(hwnd, out rect);

                // Przerabiamy na C#-owski format (left,top,right,bottom => x,y,width,height)
                nrect = new Rectangle(rect.Location, new Size(rect.Width - rect.Left, rect.Height - rect.Top));
            }
            else nrect = Screen.PrimaryScreen.Bounds;

            image = new Bitmap(nrect.Width, nrect.Height, PixelFormat.Format24bppRgb);
            graph = Graphics.FromImage(image);
            graph.CopyFromScreen(nrect.Location, new Point(0, 0), nrect.Size, CopyPixelOperation.SourceCopy);
            graph.Dispose();

            return image;
        }

        public static void ResetWindowSize()
        {
            DisplaySize = Screen.PrimaryScreen.Bounds.Size;
            if (DisplaySize.Width <= 1024 || DisplaySize.Height <= 768) DisplaySize = new Size(1024, 768);

            ScreenSize.Width = (int)(((double)DisplaySize.Width / 10.0)  );
            ScreenSize.Height = (int)(((double)DisplaySize.Height / 10.0)  );

            WindowSize.Width = ScreenSize.Width + (int)((5.0f + 50.0f) * Stuff.Scale);
            WindowSize.Height = ScreenSize.Height + (int)((7.0f + 7.0f) * Stuff.Scale);
        }

        public static void SaveImage(string filename, Image image, ScreenFormatEnum type)
        {
            ImageFormat format = (ProgramConfig.ScreenFormat==ScreenFormatEnum.PNG) ? ImageFormat.Png : ImageFormat.Jpeg;
            image.Save(filename, format);
        }
        #endregion

        public static void Beep()
        {
            if (ProgramConfig.Sounds)
            {
                PlaySound(ExeDirectory + "\\bleep.wav", IntPtr.Zero, SoundFlags.SND_FILENAME | SoundFlags.SND_ASYNC);
            }
        }

        #region HookFunction
        public static void KeyToScreen(ScreenType type)
        {
            Image image = null;

            // Local
            string local = GetCacheRandomName( "." + FormatToString(ProgramConfig.ScreenFormat) );

            string hardlocal = ProgramConfig.AutoSavePath + "\\" + DateTime.Now.ToString(ProgramConfig.NameFormat);
            hardlocal += "." + FormatToString(ProgramConfig.ScreenFormat);
            int i = 1;
            while (File.Exists(hardlocal))
            {
                hardlocal = ProgramConfig.AutoSavePath + "\\" + DateTime.Now.ToString(ProgramConfig.NameFormat) + "(" + i + ")";
                hardlocal += "." + FormatToString(ProgramConfig.ScreenFormat);
                i++;
            }

            try
            {
                bool tmpvisible = MainForm.Visible;
                MainForm.Visible = false;
                // Jesli podglad byl widoczny to czekamy 60milisekund az calkiem zniknie
                if (tmpvisible == true) Thread.Sleep(60);

                // Tworzymy screena
                image = Stuff.MakeScreen(type);

                MainForm.Visible = tmpvisible;

                // Zapisujemy zrzut ekranu i jego miniaturke
                try { SaveImage(local, image, ProgramConfig.ScreenFormat); }
                catch { throw new Exception(CreateFileFolderError(local)); }

                if (ProgramConfig.AutoSave)
                {
                    try { SaveImage(hardlocal, image, ProgramConfig.ScreenFormat); }
                    catch { throw new Exception(CreateFileFolderError(hardlocal)); }
                }
            }
            catch (Exception e)
            {
                MainForm.ShowMessage(TabString[2], TabString[0] + " : " + e.Message, ToolTipIcon.Error);
                return;
            }

            MainForm.UpdateIcon();

            Beep();

            Size newsize = CalcAspectRatio(image.Size, ScreenSize);
            MainForm.AddThumbnail(image.GetThumbnailImage(newsize.Width, newsize.Height, null, IntPtr.Zero), local, "", true);
            image.Dispose();
        }
        #endregion
    }
}
