using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace screenup
{
    public partial class Thumbnail : UserControl
    {
        private string pLocal = "";
        private string pWeb = "";
        private Image pThumbnailImage = null;
        private System.Diagnostics.Process pProcess = null;

        private int pProgress = 0;
        private int pToProgress = 0;

        private void SetWeb(string web) {
            pWeb = web;

            if (web == "") {
                Invalidate(false);
                linkLabel1.Text = "Hide";
                linkLabel2.Text = "Publish";
            }
            else if (web == "%") {
                Invalidate(false);
                linkLabel1.Text = "Please";
                linkLabel2.Text = "Wait";
                linkLabel3.Visible = false;
            }
            else {
                Invalidate(false);
                linkLabel1.Text = "Hide";
                linkLabel2.Text = "Link";
                linkLabel2.LinkColor = Color.FromArgb(0x00, 0xce, 0x49);
                linkLabel3.Visible = true;
                linkLabel3.Text = "Open";
            }
        }

        public Image ThumbnailImage {
            get { return pThumbnailImage; }
            set { pThumbnailImage = value; }
        }

        public string Local {
            get { return pLocal; }
            set { pLocal = value; }
        }

        public string Web {
            get { return pWeb; }
            set { SetWeb(value); }
        }

        public Thumbnail() {
            InitializeComponent();
            Size = Stuff.WindowSize;
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer |
                          ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor, true);
        }

        private void Thumbnail_Paint(object sender, PaintEventArgs e)  {
            Pen pen = new Pen(Color.FromArgb(170,168,155));
            int posx = 0;
            int posy = 0;
            if (ThumbnailImage != null) {
                 posx = (int)(5.0f * Stuff.Scale + (Stuff.ScreenSize.Width / 2) - (ThumbnailImage.Width / 2));
                 posy = (int)(7.0f * Stuff.Scale + (Stuff.ScreenSize.Height / 2) - (ThumbnailImage.Height / 2));

                e.Graphics.DrawImage(ThumbnailImage, posx, posy, ThumbnailImage.Width, ThumbnailImage.Height);
            }

            e.Graphics.DrawLine(pen, 5, 0, Size.Width - 5, 0);

            if (Web == "%")
            {
                Rectangle rect = new Rectangle(posx, posy, ThumbnailImage.Width, ThumbnailImage.Height);
                e.Graphics.Clip = new Region(rect);
                rect.Inflate(ThumbnailImage.Width / 3, ThumbnailImage.Height / 3); //
                int g = (int)(((float)pProgress / 100.0f) * 360.0f);
                Color c = Color.FromArgb(150, 30, 30, 30);
                e.Graphics.FillPie(new SolidBrush(c), rect, -90 + g, 360 - g);
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Web == "%") return;

            if (pWeb.Length == 0)
            {
                try
                {
                    {
                        pProcess = System.Diagnostics.Process.Start("mspaint.exe", '"' + Local + '"');
                        pProcess.Exited += new EventHandler(OnSystemEditorClose);
                        pProcess.EnableRaisingEvents = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Can't open file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                System.Diagnostics.Process.Start('"' + pWeb + '"');
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            if (Web == "%") return;

            ThumbnailImage.Dispose();
            ThumbnailImage = null;
            Stuff.MainForm.RemoveNullThumbnail();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            if (Web == "%") return;

            if (Web == "")
            {
                Web = "%";
                backgroundWorker1.RunWorkerAsync();
            }
            else
            {
                try
                {
                    if (Stuff.ProgramConfig.AutoOpen)
                    {
                        LinkBox linkbox = new LinkBox();
                        linkbox.SetWeb(Web);
                        linkbox.ShowDialog();
                    }
                    else
                    {
                        Stuff.ClipboardSetText(Web);
                        Stuff.Beep();
                    }

                    linkLabel2.LinkColor = Color.FromArgb(40, 131, 156);
                }
                catch { return; }
            }
        }

        private delegate void SetWebDelegate(string link);

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (pToProgress == e.ProgressPercentage) return;
            if (e.ProgressPercentage > 95) return;

            pToProgress = e.ProgressPercentage;
            Invalidate();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {
            try {
                string link = "";
                Stuff.PostFile("https://api.imgur.com/3/image", Local, out link, backgroundWorker1);
                this.Invoke((SetWebDelegate)SetWeb, link);

                Stuff.Beep();
            }
            catch (Exception ex) {
                this.Invoke((SetWebDelegate)SetWeb, "");
                Stuff.MainForm.ShowMessage(Stuff.TabString[2], Stuff.TabString[1] + " : " + ex.Message, ToolTipIcon.Error);
            }
        }

        private void OnSystemEditorClose(object sender, EventArgs e)
        {
            Image image = new Bitmap(pLocal);
            Size newsize = Stuff.CalcAspectRatio(image.Size, Stuff.ScreenSize);
            pThumbnailImage = image.GetThumbnailImage(newsize.Width, newsize.Height, null, IntPtr.Zero);
            image.Dispose();
            Invalidate();
        }

        private void Thumbnail_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start('"' + Local + '"');
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't open file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (pProgress == pToProgress) return;

            pProgress += 1;
            if (pProgress > pToProgress) pProgress = pToProgress;
            Invalidate();
        }

    }
}
