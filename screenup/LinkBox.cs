﻿using System;
using System.Windows.Forms;

namespace screenup
{
    public partial class LinkBox : Form
    {
        public LinkBox()
        {
            InitializeComponent();
        }

        public void SetWeb(string str) { textBox1.Text = str; }

        private void button2_Click(object sender, EventArgs e)
        {
            Stuff.ProgramConfig.AutoOpen = !checkBox1.Checked;
            Close();
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.SelectAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start('"' + textBox1.Text + '"');
            Close();
        }
    }
}
