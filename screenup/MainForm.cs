using System;
using System.IO;
using System.Net;
using System.Drawing;
using System.Reflection;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Collections.Generic;

namespace screenup
{
    public partial class MainForm : Form
    {
        private TcpListener Listener;
        private bool MakeVisible = false;

        public MainForm()
        {
            InitializeComponent();
            Stuff.Scale = (float)this.AutoScaleDimensions.Height / 96.0f;
            Stuff.ResetWindowSize();

            ResizeRedraw = true;
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer |
                          ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor, true);

            Stuff.ProgramVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            label2.Text = "ScreenUp - " + Stuff.ProgramVersion;
            Size = Stuff.WindowSize;

            label1.Visible = Stuff.ProgramConfig.ChangeFormat;

            if (Stuff.ProgramConfig.ScreenFormat == Stuff.ScreenFormatEnum.JPG)
            {
                label1.Text = "Switch to PNG format";
            }
            else
            {
                label1.Text = "Switch to JPG format";
            }

            notifyIcon1.Icon = screenup.Properties.Resources.screenupbig;

            Listener = new TcpListener(new IPAddress(new byte[] { 127, 0, 0, 1 }), 5684);
            Listener.Start();
            Listener.BeginAcceptSocket(AcceptSocket, null);
        }

        public void AcceptSocket(IAsyncResult result)
        {
            MakeVisible = true;
            Listener.BeginAcceptSocket(AcceptSocket, null);
        }

        private List<Thumbnail> ThumbnailList = new List<Thumbnail>();
        
        public void AddThumbnail(Image image, string local, string web, bool changable)
        {
            if (ThumbnailList.Count == 0)
            {
                label2.Visible = false;
            }

            Thumbnail newt = new Thumbnail();
            newt.ThumbnailImage = image;
            newt.Local = local;
            newt.Web = web;

            int coff = Stuff.ProgramConfig.ChangeFormat ? 70 : 57;
            int FutureHeight = ThumbnailList.Count * Stuff.WindowSize.Height + Dp(coff);
            int MaxHeight = Screen.PrimaryScreen.WorkingArea.Height - Dp(20);

            if (ThumbnailList.Count >= Stuff.ProgramConfig.ThumbnailCount || FutureHeight >= MaxHeight)
            {
                Controls.Remove(ThumbnailList[0]);
                ThumbnailList.RemoveAt(0);
            }

            ThumbnailList.Add(newt);
            Controls.Add(newt);
            RefreshThumbs(true);
        }

        public void RemoveNullThumbnail()
        {
            foreach (Thumbnail t in ThumbnailList)
            {
                if (t.ThumbnailImage == null)
                {
                    Controls.Remove(t);
                    ThumbnailList.Remove(t);
                    break;
                }
            }

            if (ThumbnailList.Count == 0)
            {
                label2.Visible = true;
            }

            RefreshThumbs(true);
        }

        public int Dp(int size)
        {
            return (int)((float)size * Stuff.Scale);
        }

        public void RefreshThumbs(bool all)
        {
            int coff = Stuff.ProgramConfig.ChangeFormat ? 70 : 57;

            if (Stuff.ProgramConfig.ChangeFormat)
            {
                label1.Visible = true;
            }
            else
            {
                label1.Visible = false;
            }

            if (ThumbnailList.Count == 0) this.Size = Stuff.WindowSize;
            else this.Size = new Size(Stuff.WindowSize.Width+ Dp(2), ThumbnailList.Count * Stuff.WindowSize.Height + Dp(coff));

            SetDesktopLocation(Screen.PrimaryScreen.WorkingArea.Width - Width - Dp(15), Screen.PrimaryScreen.WorkingArea.Height - Height - Dp(20));

            bool none = true;
            if (all)
            {
                int i = 0;
                foreach (Thumbnail t in ThumbnailList)
                {
                    none = false;
                    t.Location = new Point(Dp(1), Dp(coff) + (i * Stuff.WindowSize.Height) - Dp(1));
                    i++;
                    if (t.Visible == false) t.Show();
                    t.Invalidate( new Rectangle(0, 0, t.Width, 2), false );
                }
            }

            if (none)
            {
                this.Invalidate();
            }
        }

        private void Form3_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible == true) RefreshThumbs(false);
        }

        public void UpdateIcon()
        {
            notifyIcon1.Icon = notifyIcon1.Icon;
        }

        public void ShowMessage(string title, string text, ToolTipIcon icon)
        {
            notifyIcon1.ShowBalloonTip(100, title, text, icon);
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                    Visible = !Visible;
                    if (Visible == true) Activate();
            }
        }

        private void But2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIcon1.Visible = false;
            Application.Exit();
        }

        private void But1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool t = Stuff.ProgramConfig.ChangeFormat;
            PropertiesForm propertiesform = new PropertiesForm();
            propertiesform.ShowDialog();

            if (t != Stuff.ProgramConfig.ChangeFormat)
            {
                RefreshThumbs(true);
                button1.Invalidate(true);
            }

            propertiesform.Dispose();
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Stuff.IsDialog)
            {
                e.Cancel = true;
                System.Media.SystemSounds.Beep.Play();
            }
            else
            {
                if (Clipboard.ContainsImage()) But5.Enabled = true;
                else But5.Enabled = false;
            }
        }

        private void But3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox aboutbox = new AboutBox();
            aboutbox.ShowDialog();
            aboutbox.Dispose();
        }

        private void Form3_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            Stuff.Beep();
        }

        private void DropFile(string filename)
        {
            string ext = Path.GetExtension(filename).ToLower();
            string local = null;
            Image image = null;
            switch (ext)
            {
                case ".png":
                case ".jpg":
                    local = Stuff.GetCacheRandomName(ext);
                    File.Copy(filename, local);
                    image = new Bitmap(local);
                    break;
                case ".jpeg":
                case ".bmp":
                case ".tiff":
                case ".tif":
                case ".gif":
                    local = Stuff.GetCacheRandomName(Stuff.ProgramConfig.ScreenFormat == Stuff.ScreenFormatEnum.JPG ? ".jpg" : ".png");
                    image = new Bitmap(filename);
                    image.Save(local, ImageFormat.Jpeg);
                    break;

                default:
                    return;
            }
            Size newsize = Stuff.CalcAspectRatio(image.Size, Stuff.ScreenSize);
            AddThumbnail(image.GetThumbnailImage(newsize.Width, newsize.Height, null, IntPtr.Zero), local, "", false);
            image.Dispose();
        }

        private void Form3_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length == 1)
            {
                DropFile(files[0]);
            }
        }

        private void Form3_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files.Length == 1)
                {
                    string ext = Path.GetExtension(files[0]).ToLower();
                    switch (ext)
                    {
                            case ".png":
                            case ".jpg":
                            case ".jpeg":
                            case ".bmp":
                            case ".tiff":
                            case ".tif":
                            case ".gif":
                                    e.Effect = DragDropEffects.Copy;
                                    break;
                            default:
                                    e.Effect = DragDropEffects.None;
                                    break;
                    }
                }
            }
        }

        private void But4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Visible = !Visible;
            if (Visible == true) Activate();
        }

        private void But5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Clipboard.ContainsImage())
                {
                    Image image = Clipboard.GetImage();
                    string local = Stuff.GetCacheRandomName("." + Stuff.FormatToString(Stuff.ProgramConfig.ScreenFormat));
                    Stuff.SaveImage(local, image, Stuff.ProgramConfig.ScreenFormat);

                    Size newsize = Stuff.CalcAspectRatio(image.Size, Stuff.ScreenSize);
                    AddThumbnail(image.GetThumbnailImage(newsize.Width, newsize.Height, null, IntPtr.Zero), local, "", false);
                    image.Dispose();
                }
            }
            catch { }
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.FromArgb(170, 168, 155));

            Rectangle r = new Rectangle(new Point(0, 0), new Size(Size.Width - 1, Size.Height - 1));
            e.Graphics.DrawRectangle(pen, r);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                DropFile(openFileDialog1.FileName);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            Stuff.KeyToScreen(Stuff.ScreenType.Desktop);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (MakeVisible)
            {
                Visible = true;
                MakeVisible = false;
            }
        }

        private void label1_Click(object sender, MouseEventArgs e)
        {
            if (Stuff.ProgramConfig.ScreenFormat == Stuff.ScreenFormatEnum.JPG)
            {
                Stuff.ProgramConfig.ScreenFormat = Stuff.ScreenFormatEnum.PNG;
                label1.Text = "Switch to JPG format";
            }
            else
            {
                Stuff.ProgramConfig.ScreenFormat = Stuff.ScreenFormatEnum.JPG;
                label1.Text = "Switch to PNG format";
            }

            button1.Invalidate(false);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}