﻿using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace screenup
{
    public partial class ScreenUpBar : UserControl
    {
        public ScreenUpBar()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer |
                          ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor, true);
        }

        private int Dp(int v)
        {
            return (int)((float)v * Stuff.Scale + 0.5f);
        }
        private float Dpf(int v)
        {
            return ((float)v * Stuff.Scale + 0.5f);
        }

        private void ScreenUpBar_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            Rectangle rect = new Rectangle(Location, Size);
            e.Graphics.FillRectangle( new SolidBrush( Color.FromArgb(61, 61, 61) ), rect );
            e.Graphics.FillRectangle(new LinearGradientBrush(new Point(0, 0), new Point(0, Dp(10)), Color.FromArgb(100, 100, 100), Color.FromArgb(80, 80, 80)), new Rectangle(rect.Location, new Size(rect.Width, Dp(10))));

            e.Graphics.DrawImage(screenup.Properties.Resources.logo1, new Rectangle(Dp(3), Dp(3), Dp(14), Dp(14)));
            e.Graphics.DrawString("ScreenUp", Font, new SolidBrush(Color.Yellow), new PointF(Dpf(23), Dpf(3)));

            string f = "";
            if (Stuff.ProgramConfig.ChangeFormat == true)
            {
                if (Stuff.ProgramConfig.ScreenFormat == Stuff.ScreenFormatEnum.PNG) f = "PNG";
                else f = "JPG";
            }

            e.Graphics.DrawString(f, Font, new SolidBrush(Color.White), new PointF(rect.Width - Dpf(35), Dpf(3)));
        }

    }
}
