using System;
using System.Windows.Forms;
using System.IO;
using System.Net.Sockets;

namespace screenup
{
    static class Program
    {

        [STAThread]
        static void Main()
        {
            try
            {
                using (TcpClient client = new TcpClient())
                {
                    IAsyncResult result = client.BeginConnect("localhost", 5684, null, null);
                    bool success = result.AsyncWaitHandle.WaitOne(200, true);

                    if (success)
                    {
                        return;
                    }
                }
            }
            catch { }

            Hook.HookWindows();

            try
            {
                bool secondtime;
                Stuff.EmptyCache();
                secondtime = Stuff.LoadConfig();

                if (Directory.Exists(Stuff.ProgramConfig.AutoSavePath) == false)
                {
                    Directory.CreateDirectory(Stuff.ProgramConfig.AutoSavePath);
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                Stuff.MainForm = new MainForm();

                if (secondtime) Application.Run();
                else
                {
                    Application.Run(Stuff.MainForm);
                    Stuff.SaveConfig();
                }
            }
            catch (Exception e)
            {
                TextWriter tw = new StreamWriter(Stuff.AppDirectory + "\\bugreport.txt");
                tw.WriteLine(e.Message);
                tw.WriteLine(e.StackTrace);
                tw.Close();

                MessageBox.Show("ScreenUp just crashed. Sorry.", "ScreenUp", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally { Hook.UnhookWindows(); }
        }
    }
}